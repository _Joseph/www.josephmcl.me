from flask import Flask, render_template

from . import routes


@routes.route('/', methods=['GET','POST'])
def index():
    """ 
    """
    return render_template('index.html')