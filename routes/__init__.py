from flask import Blueprint
__all__ = ['index']
routes = Blueprint('routes', __name__)
from .index import *