import sys

from flask import Flask

app = Flask(__name__)

from routes import routes
app.register_blueprint(routes)


if __name__ == '__main__':

    app.run()
